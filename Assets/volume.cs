﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class volume : MonoBehaviour
{
    public AudioMixer miAudio;
    public float test = 2; 

    // Start is called before the first frame update
    void Start()
    {
        miAudio.SetFloat("Vol", PlayerPrefs.GetFloat("Volume"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

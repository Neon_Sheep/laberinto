﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;
    public VariableJoystick variableJoystick;
    public CharacterController rb;

    public void Update()
    {
        Vector3 direction = transform.right * variableJoystick.Horizontal + transform.forward * variableJoystick.Vertical;
        rb.Move(direction * speed * Time.deltaTime);
    }

    /*public void FixedUpdate()
    {
        //Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        Vector3 direction = transform.right * variableJoystick.Vertical + transform.forward * variableJoystick.Horizontal;
        rb.Move(direction * speed * Time.deltaTime);
    }*/
}
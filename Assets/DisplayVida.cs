﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayVida : MonoBehaviour
{

    public ContadorM vida;
    public Text text;
    public Text playername;
    public Text llaves;
    public Text tiempo;
    private float temp;
    // Start is called before the first frame update
    void Start()
    {
        temp = Time.time;
        playername.text = PlayerPrefs.GetString("nombre"); 
    }

    // Update is called once per frame
    void Update()
    {
        text.text = $"{vida.contador}";
        llaves.text = $"{vida.llaves}"; 
        tiempo.text = $"{(temp+120f)-Time.time}"; 
    }
}

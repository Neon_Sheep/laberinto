﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalBueno : MonoBehaviour
{
    public Text tiempo;
    public Text nombre;
    public Text tocado;
    // Start is called before the first frame update
    void Start()
    {
        int a = 10 - PlayerPrefs.GetInt("Tocado");

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        tiempo.text = $"{PlayerPrefs.GetFloat("Tiempo")*200}";
        nombre.text = $"{PlayerPrefs.GetString("nombre")}";
        tocado.text = "$Enemigos Restantes "+a;
        PlayerPrefs.SetFloat("Tiempo",0f);
        PlayerPrefs.Save();
    }
}

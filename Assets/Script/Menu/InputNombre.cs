﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InputNombre : MonoBehaviour
{
    public InputField inputField;
    public Slider miSlider;

    public void Start()
    {
        if (PlayerPrefs.HasKey("Volume")) 
            miSlider.value = PlayerPrefs.GetFloat("Volume"); 
    }

    public void GetData(int dificultad)
    {
        string cadena = "";
        cadena = inputField.text;
        PlayerPrefs.SetString("nombre", cadena);
        PlayerPrefs.SetInt("dificultad", dificultad);
        PlayerPrefs.SetFloat("Volume", miSlider.value); 
        PlayerPrefs.Save();
        if (PlayerPrefs.GetString("nombre").Length != 0)
        {
            SceneManager.LoadScene("Laberinto");
        }
    }
}

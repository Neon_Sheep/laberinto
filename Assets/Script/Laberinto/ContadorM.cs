﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ContadorM : MonoBehaviour
{
    public Transform zapato;
    public Transform Aranha1;
    public Transform Aranha2;
    public Transform Aranha3;
    public Transform Aranha4;
    public Transform Aranha5;
    public Transform Aranha6;
    public Transform Aranha7;
    public Transform Aranha8;
    public Transform Aranha9;
    public Transform Aranha10;
    public Transform Puerta;
    public int contador = 3;
    public int llaves = 0; 
    public AudioSource Hit; 
    public AudioSource apertura; 
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("Tocado", contador);
    }

    // Update is called once per frame
    void Update()
    {
        if (Aranha1 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha1.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha1.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }

        if (Aranha2 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha2.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha2.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }

        if (Aranha3 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha3.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha3.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }
        
        if (Aranha4 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha4.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha4.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }

        if (Aranha5 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha5.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha5.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }

        if (Aranha6 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha6.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha6.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }
        
        if (Aranha7 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha7.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha7.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }

        if (Aranha8 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha8.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha8.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }

        if (Aranha9 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha9.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha9.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }
        
        if (Aranha10 != null)
        {
            if (Vector3.Distance(zapato.position, Aranha10.position) < 1.5)
            {
                //Debug.Log("erai");
                Destroy(Aranha10.gameObject);
                contador--;
                Hit.Play();
                PlayerPrefs.SetInt("Tocado", contador);
            }
        }

        if (llaves == PlayerPrefs.GetInt("dificultad") && Puerta != null)
        {
            apertura.Play();
            Destroy(Puerta.gameObject);
        }

        if (contador == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }



}

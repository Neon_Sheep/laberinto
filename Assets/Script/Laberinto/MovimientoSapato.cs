﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovimientoSapato : MonoBehaviour
{
    public Transform Aranha;
    public Transform zapato;
    private NavMeshAgent sapato;
    public Material material;
    public Transform piso;
    void Start()
    {
        sapato = GetComponent<NavMeshAgent>();
        sapato.speed = 15f;
    }

    void Update()
    {
        sapato.SetDestination(Aranha.position);
    }
}

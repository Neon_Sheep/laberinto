﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class footsteps : MonoBehaviour
{
    CharacterController cc;
    public AudioSource poto; 
    void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (cc.velocity.magnitude > 2 && poto.isPlaying == false)
        {
            poto.volume = Random.Range(0.8f, 1);
            poto.pitch = Random.Range(0.8f, 1.1f);
            poto.Play();
        }
    }
}

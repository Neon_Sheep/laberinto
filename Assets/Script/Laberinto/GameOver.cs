﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{

    private float time;
    // Start is called before the first frame update
    void Start()
    {
        time = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Time.time);
        if (time+120f <= Time.time)
        {
            Debug.Log("Fin");
            SceneManager.LoadScene("GameOver");
        }
    }
}

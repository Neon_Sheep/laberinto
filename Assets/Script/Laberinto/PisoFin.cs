﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PisoFin : MonoBehaviour
{
    public Transform aranha;
    public Transform piso;
    private float tiempo;

    private void Start()
    {
        tiempo = Time.time;
    }
    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(aranha.position,piso.position) < 2.5) 
        {
            PlayerPrefs.SetFloat("Tiempo", Time.time - tiempo);
            PlayerPrefs.Save();
            SceneManager.LoadScene("Logrado");
        }
    }
}
